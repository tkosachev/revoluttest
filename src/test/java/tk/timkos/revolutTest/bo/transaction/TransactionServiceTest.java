package tk.timkos.revolutTest.bo.transaction;

import org.junit.BeforeClass;
import org.junit.Test;
import tk.timkos.revolutTest.ServiceLocator;
import tk.timkos.revolutTest.TechnicalException;
import tk.timkos.revolutTest.bo.account.Account;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * User: tkosachev
 * Date: 03.09.18
 * Time: 1:17
 */
public class TransactionServiceTest {

    @BeforeClass
    public static void init(){
        ServiceLocator.instance().clear();
        ServiceLocator.instance().registerService(TransactionService.class.getName(), new TransactionService());
    }

    @Test
    public void testCreateTransfer() throws Exception {
        long initialFromAccountBalance = 200l;
        long sum = 100l;
        Account fromAccount = new Account(initialFromAccountBalance);
        Account toAccount = new Account(0l);

        TransactionService transactionService = (TransactionService) ServiceLocator.instance().findService(TransactionService.class.getName());
        TransactionTransfer transfer = transactionService.createTransfer(fromAccount, toAccount, sum);
        assertNotNull(transfer);
        assertEquals(sum, transfer.getSum());
        assertEquals(fromAccount, transfer.getFromAccount());
        assertEquals(toAccount, transfer.getToAccount());
        assertEquals(initialFromAccountBalance - sum, fromAccount.getBalance());
        assertEquals(sum, toAccount.getBalance());
    }

    @Test (expected = TechnicalException.class)
    public void testCreateTransferNegativeSumShouldFail() throws Exception {
        long initialFromAccountBalance = 200l;
        long sum = -1l;
        Account fromAccount = new Account(initialFromAccountBalance);
        Account toAccount = new Account(0l);

        ((TransactionService)ServiceLocator.instance().findService(TransactionService.class.getName())).createTransfer(fromAccount, toAccount, sum);
    }

    @Test (expected = TechnicalException.class)
    public void testCreateTransferNullFromAccountShouldFail() throws Exception {
        long sum = 1l;
        Account toAccount = new Account(0l);

        ((TransactionService)ServiceLocator.instance().findService(TransactionService.class.getName())).createTransfer(null, toAccount, sum);
    }

    @Test (expected = TechnicalException.class)
    public void testCreateTransferNullToAccountShouldFail() throws Exception {
        long initialFromAccountBalance = 200l;
        long sum = 1l;
        Account fromAccount = new Account(initialFromAccountBalance);

        ((TransactionService)ServiceLocator.instance().findService(TransactionService.class.getName())).createTransfer(fromAccount, null, sum);
    }
}
