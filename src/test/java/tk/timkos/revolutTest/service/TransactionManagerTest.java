package tk.timkos.revolutTest.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import tk.timkos.revolutTest.ReferenceException;
import tk.timkos.revolutTest.ServiceLocator;
import tk.timkos.revolutTest.bo.DomainObject;
import tk.timkos.revolutTest.bo.account.Account;
import tk.timkos.revolutTest.bo.transaction.TransactionService;
import tk.timkos.revolutTest.bo.transaction.TransactionTransfer;
import tk.timkos.revolutTest.data.DatabaseManager;

import javax.persistence.LockModeType;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * User: tkosachev
 * Date: 11.09.18
 * Time: 0:37
 */
public class TransactionManagerTest {

    private TransactionService transactionServiceMock;
    private DatabaseManager databaseManagerMock;

    @Before
    public void init(){
        ServiceLocator.instance().clear();
        transactionServiceMock = mock(TransactionService.class);
        ServiceLocator.instance().registerService(TransactionService.class.getName(), transactionServiceMock);
        databaseManagerMock = mock(DatabaseManager.class);
        ServiceLocator.instance().registerService(DatabaseManager.class.getName(), databaseManagerMock);
    }

    @Test (expected = ReferenceException.class)
    public void testCreateTransactionWithUnknownFromAccountThrowsReferenceException() throws Exception {

        //when(transactionServiceMock.createTransfer(isA(Account.class), isA(Account.class), anyLong())).thenReturn(new TransactionTransfer());

        when(databaseManagerMock.find(any(Class.class), eq(1))).thenReturn(null);
        when(databaseManagerMock.find(any(Class.class), eq(1), any(LockModeType.class))).thenReturn(null);

        when(databaseManagerMock.find(any(Class.class), eq(2))).thenReturn(new Account());
        when(databaseManagerMock.find(any(Class.class), eq(2), any(LockModeType.class))).thenReturn(new Account());

        TransactionManager transactionManager = new TransactionManager();
        transactionManager.makeTransfer(1, 2, 100l);
    }

    @Test (expected = ReferenceException.class)
    public void testCreateTransactionWithUnknownToAccountThrowsReferenceException() throws Exception {

        //when(transactionServiceMock.createTransfer(isA(Account.class), isA(Account.class), anyLong())).thenReturn(new TransactionTransfer());

        when(databaseManagerMock.find(any(Class.class), eq(2))).thenReturn(null);
        when(databaseManagerMock.find(any(Class.class), eq(2), any(LockModeType.class))).thenReturn(null);

        when(databaseManagerMock.find(any(Class.class), eq(1))).thenReturn(new Account());
        when(databaseManagerMock.find(any(Class.class), eq(1), any(LockModeType.class))).thenReturn(new Account());

        TransactionManager transactionManager = new TransactionManager();
        transactionManager.makeTransfer(1, 2, 100l);
    }

    @Test
    public void testCreateTransaction() throws Exception {

        when(transactionServiceMock.createTransfer(isA(Account.class), isA(Account.class), anyLong())).thenReturn(new TransactionTransfer());

        Account accountFrom = new Account();
        accountFrom.setId(1);
        accountFrom.setBalance(100l);

        Account accountTo = new Account();
        accountFrom.setId(2);
        accountFrom.setBalance(0l);

        when(databaseManagerMock.find(any(Class.class), eq(1))).thenReturn(accountFrom);
        when(databaseManagerMock.find(any(Class.class), eq(1), any(LockModeType.class))).thenReturn(accountFrom);

        when(databaseManagerMock.find(any(Class.class), eq(2))).thenReturn(accountTo);
        when(databaseManagerMock.find(any(Class.class), eq(2), any(LockModeType.class))).thenReturn(accountTo);

        TransactionManager transactionManager = new TransactionManager();
        transactionManager.makeTransfer(1, 2, 100l);

        verify(databaseManagerMock).save((DomainObject) ArgumentMatchers.notNull());
        verify(transactionServiceMock).createTransfer(accountFrom, accountTo, 100l);
    }
}
