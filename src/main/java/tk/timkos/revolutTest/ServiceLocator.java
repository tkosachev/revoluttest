package tk.timkos.revolutTest;

import java.util.HashMap;
import java.util.Map;

/**
 * User: tkosachev
 * Date: 13.09.18
 * Time: 0:34
 */
public class ServiceLocator {

    private static final ServiceLocator instance = new ServiceLocator();

    public static ServiceLocator instance(){
        return instance;
    }

    private Map<String, Object> services = new HashMap<String, Object>();

    public void clear(){
        services.clear();
    }

    public void registerService (String name, Object service){
        services.put(name, service);
    }

    public Object findService(String name){
        return services.get(name);
    }
}
