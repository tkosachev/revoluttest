package tk.timkos.revolutTest.data;

import org.eclipse.persistence.config.PersistenceUnitProperties;

import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

/**
 * User: tkosachev
 * Date: 08.09.18
 * Time: 23:39
 */
public class Persistence {

    public static EntityManagerFactory createEMF(boolean replaceTables) {
        Map<String, Object> props = new HashMap<String, Object>();

        props.put(PersistenceUnitProperties.NON_JTA_DATASOURCE, "");
        props.put(PersistenceUnitProperties.JTA_DATASOURCE, "");
        props.put(PersistenceUnitProperties.TRANSACTION_TYPE, "RESOURCE_LOCAL");

        props.put(PersistenceUnitProperties.WEAVING, "false");

        if (replaceTables) {
            props.put(PersistenceUnitProperties.DDL_GENERATION, PersistenceUnitProperties.CREATE_OR_EXTEND);
            props.put(PersistenceUnitProperties.DDL_GENERATION_MODE, PersistenceUnitProperties.DDL_DATABASE_GENERATION);
        }
        return javax.persistence.Persistence.createEntityManagerFactory("revolutTest", props);
    }

}
