package tk.timkos.revolutTest.data;

import tk.timkos.revolutTest.bo.DomainObject;

import javax.persistence.LockModeType;

/**
 * User: tkosachev
 * Date: 13.09.18
 * Time: 1:41
 */
public interface DatabaseManager {
    void start();

    void beginTransaction();

    void commit();

    void rollback();

    <T extends DomainObject> T find(Class<T> c, int id);

    <T extends DomainObject> T find(Class<T> c, int id, LockModeType lockModeType);

    void save(DomainObject o);
}
