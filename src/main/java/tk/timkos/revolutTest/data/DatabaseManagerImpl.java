package tk.timkos.revolutTest.data;

import tk.timkos.revolutTest.bo.DomainObject;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.LockModeType;

/**
 * User: tkosachev
 * Date: 03.09.18
 * Time: 4:00
 */
public class DatabaseManagerImpl implements DatabaseManager {
    public DatabaseManagerImpl() {
        factory = Persistence.createEMF(true);
    }

    private EntityManagerFactory factory;
    private ThreadLocal<EntityManager> entityManager = new ThreadLocal<EntityManager>();

    @Override
    public void start(){
        //TODO: check if entityManager.get() != null, and react accordingly
        entityManager.set(factory.createEntityManager());
    }

    @Override
    public void beginTransaction(){
        entityManager.get().getTransaction().begin();
    }

    @Override
    public void commit(){
        entityManager.get().getTransaction().commit();
        entityManager.get().close();
        entityManager.set(null);
    }

    @Override
    public void rollback(){
        entityManager.get().getTransaction().rollback();
        entityManager.get().close();
        entityManager.set(null);
    }

    @Override
    public <T extends DomainObject> T find(Class<T> c, int id) {
        return entityManager.get().find(c, id);
    }

    @Override
    public <T extends DomainObject> T find(Class<T> c, int id, LockModeType lockModeType) {
        return entityManager.get().find(c, id, lockModeType);
    }

    @Override
    public void save(DomainObject o){
        entityManager.get().persist(o);
    }
}
