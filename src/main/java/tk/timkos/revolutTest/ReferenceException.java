package tk.timkos.revolutTest;

/**
 * User: tkosachev
 * Date: 10.09.18
 * Time: 0:43
 */
public class ReferenceException extends Exception {
    private Class refClass;
    private int id;

    public ReferenceException(Class refClass, int id) {
        this.refClass = refClass;
        this.id = id;
    }

    public Class getRefClass() {
        return refClass;
    }

    public void setRefClass(Class refClass) {
        this.refClass = refClass;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
