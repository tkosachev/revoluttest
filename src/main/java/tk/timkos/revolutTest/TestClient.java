package tk.timkos.revolutTest;

import org.glassfish.jersey.client.ClientConfig;
import tk.timkos.revolutTest.bo.account.Account;
import tk.timkos.revolutTest.bo.transaction.TransactionTransfer;
import tk.timkos.revolutTest.web.AccountResponse;
import tk.timkos.revolutTest.web.CreateAccountRequest;
import tk.timkos.revolutTest.web.CreateTransferRequest;
import tk.timkos.revolutTest.web.TransferResponse;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * User: tkosachev
 * Date: 13.09.18
 * Time: 2:52
 */
public class TestClient {
    public static void main(String[] args){
        Client client = ClientBuilder.newClient(new ClientConfig());
        WebTarget webTarget = client.target("http://localhost:9998/revolutTest").path("account").path("create");

        CreateAccountRequest request = new CreateAccountRequest();
        request.setBalance(100l);

        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(request, MediaType.APPLICATION_JSON));

        System.out.println(response.getStatus());
        AccountResponse accountResponse1 = response.readEntity(AccountResponse.class);
        Account accountFrom = accountResponse1.getAccount();
        System.out.println("AccountFrom.id = " + accountFrom.getId());

        request.setBalance(0l);

        response = invocationBuilder.post(Entity.entity(request, MediaType.APPLICATION_JSON));

        System.out.println(response.getStatus());
        AccountResponse accountResponse2 = response.readEntity(AccountResponse.class);
        Account accountTo = accountResponse2.getAccount();
        System.out.println("AccountTo.id = " + accountTo.getId());

        CreateTransferRequest request2 = new CreateTransferRequest();
        request2.setAccountFromId(accountFrom.getId());
        request2.setAccountToId(accountTo.getId());
        request2.setSum(100l);

        webTarget = client.target("http://localhost:9998/revolutTest").path("transaction").path("transfer").path("create");
        invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
        response = invocationBuilder.post(Entity.entity(request2, MediaType.APPLICATION_JSON));

        System.out.println(response.getStatus());
        TransferResponse transferResponse = response.readEntity(TransferResponse.class);
        TransactionTransfer transactionTransfer = transferResponse.getTransfer();
        System.out.println("AccountFrom.balance = " + transactionTransfer.getFromAccount().getBalance());
        System.out.println("AccountTo.balance = " + transactionTransfer.getToAccount().getBalance());
        System.out.println("Transaction.date = " + transactionTransfer.getDate());

    }
}
