package tk.timkos.revolutTest.bo.transaction;

import tk.timkos.revolutTest.bo.account.Account;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

/**
 * User: tkosachev
 * Date: 02.09.18
 * Time: 19:10
 */
@Entity
public class TransactionTransfer extends Transaction {

    @ManyToOne
    @JoinColumn(name = "from_account_id")
    private Account fromAccount;
    @ManyToOne
    @JoinColumn(name = "to_account_id")
    private Account toAccount;

    private long sum;

    private Date date;



    public Account getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(Account fromAccount) {
        this.fromAccount = fromAccount;
    }

    public Account getToAccount() {
        return toAccount;
    }

    public void setToAccount(Account toAccount) {
        this.toAccount = toAccount;
    }

    public long getSum() {
        return sum;
    }

    public void setSum(long amount) {
        this.sum = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
