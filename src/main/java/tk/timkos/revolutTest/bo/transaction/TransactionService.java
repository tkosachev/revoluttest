package tk.timkos.revolutTest.bo.transaction;

import tk.timkos.revolutTest.BusinessException;
import tk.timkos.revolutTest.TechnicalException;
import tk.timkos.revolutTest.bo.account.Account;

import java.util.Date;

/**
 * User: tkosachev
 * Date: 09.09.18
 * Time: 13:14
 *
 * Performs internal transaction-related actions.
 * No persistence here
 *
 */
public class TransactionService {

    public TransactionTransfer createTransfer(Account accountFrom, Account accountTo, long sum) throws BusinessException, TechnicalException {
        if(accountFrom == null){
            throw new TechnicalException("Null account from");
        }
        if(accountTo == null){
            throw new TechnicalException("Null account to");
        }
        if(sum < 0){
            throw new TechnicalException("Negative sum");
        }
        if(accountFrom.getBalance() < sum){
            throw new BusinessException("Balance not enough");
        }
        accountFrom.setBalance(accountFrom.getBalance() - sum);
        accountTo.setBalance(accountTo.getBalance() + sum);
        TransactionTransfer newTransfer = new TransactionTransfer();
        newTransfer.setSum(sum);
        newTransfer.setFromAccount(accountFrom);
        newTransfer.setToAccount(accountTo);
        newTransfer.setDate(new Date());
        return newTransfer;
    }
}
