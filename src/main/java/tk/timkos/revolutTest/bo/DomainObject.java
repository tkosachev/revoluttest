package tk.timkos.revolutTest.bo;

import javax.persistence.*;

/**
 * User: tkosachev
 * Date: 02.09.18
 * Time: 18:01
 */
@Entity
public abstract class DomainObject {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
