package tk.timkos.revolutTest.bo.account;

import tk.timkos.revolutTest.bo.DomainObject;

import javax.persistence.Entity;

/**
 * User: tkosachev
 * Date: 02.09.18
 * Time: 18:36
 */
@Entity
public class Account extends DomainObject {

    private long balance;

    public Account() {
    }

    public Account(long balance) {
        this.balance = balance;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }
}
