package tk.timkos.revolutTest.bo.account;

/**
 * User: tkosachev
 * Date: 09.09.18
 * Time: 13:01
 *
 * Performs internal account-related actions.
 * No persistence here
 *
 */
public class AccountService {

    public Account createAccount(){
        return new Account(0l);
    }

    public Account createAccount(long initialBalance){
        return new Account(initialBalance);
    }
}
