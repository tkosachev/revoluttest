package tk.timkos.revolutTest.service;

import tk.timkos.revolutTest.ReferenceException;
import tk.timkos.revolutTest.ServiceLocator;
import tk.timkos.revolutTest.bo.account.Account;
import tk.timkos.revolutTest.bo.account.AccountService;
import tk.timkos.revolutTest.data.DatabaseManager;

/**
 * User: tkosachev
 * Date: 11.09.18
 * Time: 2:01
 */
public class AccountManager {

    public Account createAccount(Long balance){
        DatabaseManager databaseManager = (DatabaseManager) ServiceLocator.instance().findService(DatabaseManager.class.getName());
        databaseManager.start();
        databaseManager.beginTransaction();
        Account account;
        if(balance != null){
            account = ((AccountService) ServiceLocator.instance().findService(AccountService.class.getName())).createAccount(balance);
        } else {
            account = ((AccountService) ServiceLocator.instance().findService(AccountService.class.getName())).createAccount();
        }
        databaseManager.save(account);
        databaseManager.commit();
        return account;
    }

    public Account findAccount(int id) throws ReferenceException {
        DatabaseManager databaseManager = (DatabaseManager) ServiceLocator.instance().findService(DatabaseManager.class.getName());
        databaseManager.start();
        databaseManager.beginTransaction();
        Account account = databaseManager.find(Account.class, id);
        if(account == null){
            databaseManager.rollback();
            throw new ReferenceException(Account.class, id);
        }
        databaseManager.commit();
        return account;
    }
}
