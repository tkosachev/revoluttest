package tk.timkos.revolutTest.service;

import tk.timkos.revolutTest.BusinessException;
import tk.timkos.revolutTest.ReferenceException;
import tk.timkos.revolutTest.ServiceLocator;
import tk.timkos.revolutTest.TechnicalException;
import tk.timkos.revolutTest.bo.account.Account;
import tk.timkos.revolutTest.bo.transaction.TransactionService;
import tk.timkos.revolutTest.bo.transaction.TransactionTransfer;
import tk.timkos.revolutTest.data.DatabaseManager;

import javax.persistence.LockModeType;

/**
 * User: tkosachev
 * Date: 09.09.18
 * Time: 1:05
 */
public class TransactionManager {

    public TransactionTransfer makeTransfer(int accountFromId, int accountToId, long sum) throws ReferenceException, BusinessException, TechnicalException {
        DatabaseManager databaseManager = (DatabaseManager) ServiceLocator.instance().findService(DatabaseManager.class.getName());
        databaseManager.start();
        databaseManager.beginTransaction();
        Account accountFrom = databaseManager.find(Account.class, accountFromId, LockModeType.PESSIMISTIC_WRITE);
        if(accountFrom == null){
            throw new ReferenceException(Account.class, accountFromId);
        }
        Account accountTo = databaseManager.find(Account.class, accountToId, LockModeType.PESSIMISTIC_WRITE);
        if(accountTo == null){
            throw new ReferenceException(Account.class, accountToId);
        }
        TransactionTransfer transactionTransfer = ((TransactionService)ServiceLocator.instance().findService(TransactionService.class.getName())).createTransfer(accountFrom, accountTo, sum);
        databaseManager.save(transactionTransfer);
        databaseManager.commit();
        return transactionTransfer;
    }
}
