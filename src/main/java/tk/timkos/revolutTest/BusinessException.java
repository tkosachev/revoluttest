package tk.timkos.revolutTest;

/**
 * User: tkosachev
 * Date: 10.09.18
 * Time: 23:30
 */
public class BusinessException extends Exception {

    public BusinessException(String message) {
        super(message);
    }
}
