package tk.timkos.revolutTest.web;

import tk.timkos.revolutTest.bo.transaction.TransactionTransfer;

/**
 * User: tkosachev
 * Date: 13.09.18
 * Time: 3:25
 */
public class TransferResponse extends BaseWebResponse {
    private TransactionTransfer transfer;

    public TransferResponse() {
    }

    public TransferResponse(TransactionTransfer transfer) {
        this.transfer = transfer;
    }

    public TransferResponse(String error, int errorCode) {
        super(error, errorCode);
    }

    public TransactionTransfer getTransfer() {
        return transfer;
    }

    public void setTransfer(TransactionTransfer transfer) {
        this.transfer = transfer;
    }
}
