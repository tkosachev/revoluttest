package tk.timkos.revolutTest.web;

import tk.timkos.revolutTest.bo.account.Account;

/**
 * User: tkosachev
 * Date: 13.09.18
 * Time: 3:23
 */
public class AccountResponse extends BaseWebResponse {
    private Account account;

    public AccountResponse() {
    }

    public AccountResponse(Account account) {
        this.account = account;
    }

    public AccountResponse(String error, int errorCode) {
        super(error, errorCode);
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
