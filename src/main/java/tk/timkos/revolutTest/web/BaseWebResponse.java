package tk.timkos.revolutTest.web;

/**
 * User: tkosachev
 * Date: 10.09.18
 * Time: 1:08
 */
public class BaseWebResponse {

    public static final int ERROR_REF = 1;
    public static final int ERROR_BUSINESS = 2;
    public static final int ERROR_TECH = 3;

    private String error;
    private int errorCode;

    public BaseWebResponse() {
    }

    public BaseWebResponse(String error, int errorCode) {
        this.error = error;
        this.errorCode = errorCode;
    }

        public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}