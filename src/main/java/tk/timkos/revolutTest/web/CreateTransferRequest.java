package tk.timkos.revolutTest.web;

/**
 * User: tkosachev
 * Date: 10.09.18
 * Time: 0:18
 */
public class CreateTransferRequest {
    private int accountFromId;
    private int accountToId;
    private long sum;

    public int getAccountFromId() {
        return accountFromId;
    }

    public void setAccountFromId(int accountFromId) {
        this.accountFromId = accountFromId;
    }

    public int getAccountToId() {
        return accountToId;
    }

    public void setAccountToId(int accountToId) {
        this.accountToId = accountToId;
    }

    public long getSum() {
        return sum;
    }

    public void setSum(long sum) {
        this.sum = sum;
    }
}
