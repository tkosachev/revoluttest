package tk.timkos.revolutTest.web;

import tk.timkos.revolutTest.BusinessException;
import tk.timkos.revolutTest.ReferenceException;
import tk.timkos.revolutTest.ServiceLocator;
import tk.timkos.revolutTest.TechnicalException;
import tk.timkos.revolutTest.bo.transaction.TransactionTransfer;
import tk.timkos.revolutTest.service.TransactionManager;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * User: tkosachev
 * Date: 03.09.18
 * Time: 4:32
 */
@Path("/transaction")
public class TransactionHandler {

    @POST
    @Path("/transfer/create")
    @Produces("application/json")
    @Consumes("application/json")
    public TransferResponse createTransfer(CreateTransferRequest createTransferRequest) {
        System.out.println("Handling request with request = " + createTransferRequest);
        TransactionTransfer transactionTransfer;
        try {
            transactionTransfer = ((TransactionManager) ServiceLocator.instance().findService(TransactionManager.class.getName())).makeTransfer(
                    createTransferRequest.getAccountFromId(),
                    createTransferRequest.getAccountToId(),
                    createTransferRequest.getSum());
        } catch (ReferenceException e) {
            return new TransferResponse("Reference not found: " + e.getRefClass().getName() + "#" + e.getId(), BaseWebResponse.ERROR_REF);
        } catch (BusinessException e) {
            return new TransferResponse("Cannot create transfer: " + e.getMessage(), BaseWebResponse.ERROR_BUSINESS);
        } catch (TechnicalException e) {
            return new TransferResponse("Cannot create transfer: " + e.getMessage(), BaseWebResponse.ERROR_TECH);
        }
        return new TransferResponse(transactionTransfer);
    }
}
