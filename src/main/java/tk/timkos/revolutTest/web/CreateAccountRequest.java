package tk.timkos.revolutTest.web;

/**
 * User: tkosachev
 * Date: 13.09.18
 * Time: 2:25
 */
public class CreateAccountRequest {
    private Long balance;

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }
}
