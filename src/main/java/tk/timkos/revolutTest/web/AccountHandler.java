package tk.timkos.revolutTest.web;

import tk.timkos.revolutTest.ReferenceException;
import tk.timkos.revolutTest.ServiceLocator;
import tk.timkos.revolutTest.service.AccountManager;

import javax.ws.rs.*;

/**
 * User: tkosachev
 * Date: 11.09.18
 * Time: 1:46
 */
@Path("/account")
public class AccountHandler {

    @POST
    @Path("/create")
    @Produces("application/json")
    @Consumes("application/json")
    public AccountResponse createAccount(CreateAccountRequest createAccountRequest){
        return new AccountResponse(((AccountManager)ServiceLocator.instance().findService(AccountManager.class.getName())).createAccount(createAccountRequest.getBalance()));
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public AccountResponse findAccount(@PathParam("id") int id){
        try {
            return new AccountResponse(((AccountManager)ServiceLocator.instance().findService(AccountManager.class.getName())).findAccount(id));
        } catch (ReferenceException e) {
            return new AccountResponse("Error: " + e.getMessage(), BaseWebResponse.ERROR_REF);
        }
    }
}
