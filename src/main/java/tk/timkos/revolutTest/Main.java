package tk.timkos.revolutTest;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import tk.timkos.revolutTest.bo.account.AccountService;
import tk.timkos.revolutTest.bo.transaction.TransactionService;
import tk.timkos.revolutTest.data.DatabaseManager;
import tk.timkos.revolutTest.data.DatabaseManagerImpl;
import tk.timkos.revolutTest.service.AccountManager;
import tk.timkos.revolutTest.service.TransactionManager;

import java.io.IOException;
import java.net.URI;

/**
 * Main class.
 *
 */
public class Main {
    // Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI = "http://localhost:9998/revolutTest/";

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        // create a resource config that scans for JAX-RS resources and providers
        // in $package package
        final ResourceConfig rc = new ResourceConfig().packages("tk.timkos.revolutTest.web");

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    public static void main(String[] args) throws IOException {

        ServiceLocator.instance().registerService(DatabaseManager.class.getName(), new DatabaseManagerImpl());
        ServiceLocator.instance().registerService(AccountService.class.getName(), new AccountService());
        ServiceLocator.instance().registerService(TransactionService.class.getName(), new TransactionService());
        ServiceLocator.instance().registerService(AccountManager.class.getName(), new AccountManager());
        ServiceLocator.instance().registerService(TransactionManager.class.getName(), new TransactionManager());

        final HttpServer server = startServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl\nHit enter to stop it...", BASE_URI));
        System.in.read();
        server.stop();
    }
}

