package tk.timkos.revolutTest;

/**
 * User: tkosachev
 * Date: 10.09.18
 * Time: 23:49
 */
public class TechnicalException extends Exception {
    public TechnicalException(String message) {
        super(message);
    }
}
